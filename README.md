# Astronautics Terminology Catalog
A listing, comparison and examination of terms and definitions in astronautics (spaceflight).

## About
This provides a living catalog of broad but key terms and their definitions associated with the disciplines of spaceflight, orbital debris & space situational awareness (SSA), space domain awareness, and space traffic management (STM). The catalog, in MS Excel format, provides a listing of terms (with definitions from various sources) such as 'astronautics', 'spaceflight', 'orbital debris', 'ssa', 'stm', etc. 
This is a task in the author's [Astonautical Knowledge Modeling project](https://purl.org/space-ontology), and related ontology projects by Robert Rovetto.
- If you find value in my work, please [support/donate here](https://gogetfunding.com/knowledge-organization-services-ontology-terminology-metadata-concept-analysis/).

## Goals
- to provide a non-exhaustive catalog of astronautical terms and definitions as a general reference
- a semantic and conceptual analysis to help inform the development of ontologies for astronautics (see other repositories by rrovetto), and to help improve limitations of existing knowledge organization systems.
- to identify inaccuracies and errors in existing terms and definitions across documents/sources/organizations, and contribute to their improvement.
- to consist, coherent, and accurate knowledge model or voabulary, where desired. 

## Access
- The working catalog (in Microsoft Excel) is located [at this link](https://drive.google.com/file/d/1VxThyvuY_VzVl_VNan9cqTsdI6NrYWgX/view?usp=sharing)
- The file(s) is expected to be added to this repository (TBD).

[Photo of catalog (MS Excel spreadsheet)](https://raw.githubusercontent.com/rrovetto/Astronautics-Terminology/master/photos/Pic_AstronauticalCatalog1.JPG)

## How you can help / Desiderata
- This and associated projects by the author have been out-of-pocket while both volunteering for the community and needing an income and study opportunity to continue studies. So please Consider sponsoring or donating via the [sponsorship link shaped like a heart on this page](https://gogetfunding.com/knowledge-organization-services-ontology-terminology-metadata-concept-analysis/).
- Seeking formal collaborations
- Seeking professors interested in the author as a student
- However you would like to help

## Author
Robert J. Rovetto
rrovetto(at)terpalum.umd.edu
* NASA Datanauts (open data group in the Office of Chief Information Officer)
* Center for Orbital Debris Education & Research (CODER), Univ. Maryland. (Research Affiliate)
* AIAA Space Traffic Management Working Group (Co-lead on Lexicon Task)
* AIAA Space Architecture Committee on Standards (Committee member)
* IAF  Space Traffic Management Technical Committee. IAF Space Debris Committee. (committee member)
* CCSDS (Committee member)
* Journal of Search and Rescue (webmaster, journal developer)
* U.S. Merchant Mariner (Master / Deck Officer - 100 ton)

## Rights
© 2018-2020, Robert John Rovetto.
Not authorized for commercial use unless negotiated with the author.
